#!/bin/bash

echo "=> Performing database migrations..."
python manage.py migrate

echo "=> Collecting static files..."
python manage.py collectstatic --noinput

echo "=> Compiling translations..."
python manage.py compilemessages

echo "=> Starting webserver..."
if [[ "${DJANGO_SETTINGS_MODULE}" =~ "production" ]]; then
  gunicorn --bind 0.0.0.0:8000 applicationname.wsgi:application
else
  python manage.py runserver 0.0.0.0:8000
fi

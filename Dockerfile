FROM python:3.7-alpine as base
ENV PYTHONUNBUFFERED 1
ARG ENVIRONMENT=local
ENV ENVIRONMENT=$ENVIRONMENT

ENV HOME=/home/app
ENV SRC_HOME=$HOME/app
ENV APP_HOME=$HOME/web

FROM base as builder

WORKDIR $SRC_HOME

COPY requirements/base.txt requirements/$ENVIRONMENT.txt ./

RUN set -ex && \
   apk add --no-cache --virtual .build-deps \
   python3-dev build-base mariadb-dev

RUN pip wheel --no-cache-dir --no-deps --wheel-dir $SRC_HOME/wheels -r $ENVIRONMENT.txt

FROM base

WORKDIR $APP_HOME

RUN mkdir -p $APP_HOME/static && mkdir -p $APP_HOME/uploads
ADD https://github.com/Cambalab/docker-compose-wait/releases/download/2.6.0/wait /wait
RUN chmod +x /wait

RUN apk update && apk add bash gettext mariadb-dev
COPY --from=builder $SRC_HOME/wheels /wheels
COPY --from=builder $SRC_HOME/$ENVIRONMENT.txt .
RUN pip install --no-cache /wheels/*

COPY . $APP_HOME

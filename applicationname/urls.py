"""applicationname URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.apps import apps
from django.conf import settings
from django.conf.urls import include, url
from django.conf.urls.static import static
from django.views.generic import RedirectView
from django.conf.urls.i18n import i18n_patterns
from rest_framework import routers
# from apps.myModel1 import views as myModel1Views
# from apps.myModel2 import views as myModel2Views
# from apps.myModel3 import views as myModel3Views

# Routers provide an easy way of automatically determining the URL conf.
ROUTER = routers.DefaultRouter()
# ROUTER.register('myModel1', myModel1.MyModel1ViewSet)
# ROUTER.register('myModel2', myModel2.MyModel2ViewSet)
# ROUTER.register('myModel3', myModel3.MyModel3ViewSet)

urlpatterns = i18n_patterns(
    url(r'^$', RedirectView.as_view(url='/admin/')),
    path('admin/', admin.site.urls)
) + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
# urlpatterns = urlpatterns + [
#     url(r'^api/', include(ROUTER.urls)),
# ]
if settings.DEBUG:
    from django.views.defaults import page_not_found
    urlpatterns += i18n_patterns(
        url(r'^404/$', page_not_found),
    )

# Only enable debug toolbar if it's an installed app
if apps.is_installed('debug_toolbar'):
    import debug_toolbar
    urlpatterns += [
        url(r'^__debug__/', include(debug_toolbar.urls)),
    ]

if apps.is_installed('drf_yasg'):
    from drf_yasg.views import get_schema_view
    from drf_yasg import openapi
    from rest_framework import permissions

    schema_view = get_schema_view(
        openapi.Info(
            title="Snippets API",
            default_version='v1',
            description="Test description",
            terms_of_service="https://www.google.com/policies/terms/",
            contact=openapi.Contact(email="contact@snippets.local"),
            license=openapi.License(name="BSD License"),
        ),
        public=True,
        permission_classes=(permissions.AllowAny,),
    )

    urlpatterns = urlpatterns + [
        url(r'^swagger(?P<format>\.json|\.yaml)$', schema_view.without_ui(cache_timeout=0), name='schema-json'),
        url(r'^swagger/$', schema_view.with_ui('swagger', cache_timeout=0), name='schema-swagger-ui'),
        url(r'^redoc/$', schema_view.with_ui('redoc', cache_timeout=0), name='schema-redoc'),
    ]

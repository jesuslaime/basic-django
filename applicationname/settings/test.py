from .base import *  # noqa

DEBUG = True

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': 'test_applicationname_backend',
    }
}

SECRET_KEY = 'camba420'

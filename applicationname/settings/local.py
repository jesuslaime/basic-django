import os
from importlib import import_module
from .base import *

DEBUG = True
TEMPLATES[0]['OPTIONS']['debug'] = True

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': os.environ.get('MY_APP_NAME_DB_NAME'),
        'USER': os.environ.get('MY_APP_NAME_DB_USER'),
        'PASSWORD': os.environ.get('MY_APP_NAME_DB_PASS'),
        'HOST': os.environ.get('MY_APP_NAME_DB_HOST'),
        'PORT': os.environ.get('MY_APP_NAME_DB_PORT'),
    }
}

INTERNAL_IPS = ['127.0.0.1']
ALLOWED_HOSTS = ['*']

INSTALLED_APPS += [
    'django_extensions',
]

# Use vanilla StaticFilesStorage to allow tests to run outside of tox easily
STATICFILES_STORAGE = 'django.contrib.staticfiles.storage.StaticFilesStorage'

SECRET_KEY = 'applicationname'

EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'

# Django debug toolbar - show locally unless DISABLE_TOOLBAR is enabled with environment vars
# eg. DISABLE_TOOLBAR=1 ./manage.py runserver
if not os.environ.get('DISABLE_TOOLBAR'):
    INSTALLED_APPS += [
        'debug_toolbar',
    ]

    MIDDLEWARE = [
        'debug_toolbar.middleware.DebugToolbarMiddleware',
    ] + MIDDLEWARE

if os.environ.get('API_URL'):
    API_URL = os.environ.get('API_URL')

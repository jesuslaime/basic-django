from .base import *  # noqa

ALLOWED_HOSTS = os.environ.get('ALLOWED_HOSTS', '').split(' ')

DEBUG = True

# Use cached templates in production
TEMPLATES[0]['APP_DIRS'] = False
TEMPLATES[0]['OPTIONS']['loaders'] = [
    (
        'django.template.loaders.cached.Loader', [
            'django.template.loaders.filesystem.Loader',
            'django.template.loaders.app_directories.Loader',
        ]
    ),
]

# SSL required for session/CSRF cookies
CSRF_COOKIE_SECURE = True
SESSION_COOKIE_SECURE = True

URL_SCHEME = 'https'

MIDDLEWARE += [
    'whitenoise.middleware.WhiteNoiseMiddleware',
]

if os.environ.get('API_URL'):
    API_URL = os.environ.get('API_URL')

if os.environ.get('DOCKER_COMPOSE'):
    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.mysql',
            'NAME': os.environ.get('MY_APP_NAME_DB_NAME'),
            'USER': os.environ.get('MY_APP_NAME_DB_USER'),
            'PASSWORD': os.environ.get('MY_APP_NAME_DB_PASS'),
            'HOST': os.environ.get('MY_APP_NAME_DB_HOST'),
            'PORT': os.environ.get('MY_APP_NAME_DB_PORT'),
        }
    }
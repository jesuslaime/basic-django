# Django Backend (template)

## Environment Requirements

+ python 3
+ mysql-server
+ mysql-client
+ [virtualenv](https://virtualenv.pypa.io/en/latest/)
+ [virtualenvwrapper](https://virtualenvwrapper.readthedocs.io/en/latest/)
+ setup a virtualenv:

```bash
mkvirtualenvwrapper a-name-for-your-virtual-env
```

## Development Environment

### Installation

***Set the virtual environment***

```bash
workon a-name-for-your-virtual-env
```

***Install server requirements***

```bash
cd requirements
pip install -r local.txt
```

### Configuration

#### First steps

**Before applying migrations, creating superusers or running the server, a SQL database must be created.**

First, you need to expose the following environment variables to configure the django database settings.

Export values to the current terminal.
> Values have to be exported every time you open a new terminal

```bash
export MY_APP_NAME_DB_NAME="my_application_django_database_name"
export MY_APP_NAME_DB_USER="mySqlUsername"
export MY_APP_NAME_DB_PASS="mySqlPassword"
export MY_APP_NAME_DB_HOST="0.0.0.0"
export MY_APP_NAME_DB_PORT=3306
```

You can also place those values in the `~/.bashrc`.

> Once they're in the .bashrc there's no need to export them every time you open a terminal

```bash
echo "
# === My App Name Env ===
export MY_APP_NAME_DB_NAME="my_application_django_database_name"
export MY_APP_NAME_DB_USER="mySqlUsername"
export MY_APP_NAME_DB_PASS="mySqlPassword"
export MY_APP_NAME_DB_HOST="0.0.0.0"
export MY_APP_NAME_DB_PORT=3306
" >> ~/.bashrc
```

#### Create database

Once credentials are declared in a terminal or in the .bashrc we can run:

```bash
mysqladmin -u root -p create $MY_APP_NAME_DB_NAME
```

#### Make migrations

```bash
python manage.py makemigrations
```

#### Run migrations

```bash
python manage.py migrate
```

> If you notice some of the admin text fields look weird that's because you
> might need to run the compile messages script described in the
> **Other commands** section in this file.

#### Make scripts

```bash
# Shows a help dialog
make help
# Wipes the database
make nuke
# Resets your local environment. Useful after switching branches, etc.
make reset
# Executes nuke and reset scripts, then loads fixtures
make nuke-reset
# Like reset but without the wiping installed dependencies.
make clear
# Runs test suites
make test
```

> **Superusers from fixture have the following credentials:**  
> email: your @camba email  
> password: camba  

#### Other commands

***Create super user***

Note that you could have users automatically created if you add them to the accounts fixture.

```bash
# prompts the cli to create a superuser
python manage.py createsuperuser
# compiles i18n translations
python manage.py compilemessages
```

### Running the server

***declare mysql credentials in your terminal or .bashrc***

```bash
export DB_USER=yourMySqlUser
export DB_PASSWORD=yourMySqlPassword
```

***start server normally***

```bash
python manage.py runserver
```

***using [werkzeug](https://werkzeug.palletsprojects.com/en/0.15.x/tutorial/)***

```bash
python manage.py runserver_plus
```

### Development Workflow

#### Adding new dependencies

New packages can be added using `pip`

```bash
pip install package_name
```

Whenever we add a new package we **must** freeze versions

```bash
pip freeze
```

Search the new added dependency and include it to the proper requirements file.

```txt
dj-database-url==0.5.0
Django==2.2.5
django-debug-toolbar==2.0
django-extensions==2.2.1
django-mysql==3.2.0
djangorestframework==3.10.3
mysqlclient==1.4.4
pytz==2019.3
six==1.12.0
sqlparse==0.3.0
Werkzeug==0.15.6
```

#### Requirements

+ base: dependencies used through the whole project
+ local: dependencies for local usage (e.g.: debugging tools)
+ production: dependencies used for production (e.g.: production server, static files management, etc.)
+ testing: dependencies for testing only (e.g.: testing framework)
